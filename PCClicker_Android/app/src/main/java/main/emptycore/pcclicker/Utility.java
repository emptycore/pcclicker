package main.emptycore.pcclicker;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import java.io.FileDescriptor;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utility {
	public String getDate(String date, String dateFormat) {
		long milliSeconds = Long.parseLong(date);

	    SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

	     Calendar calendar = Calendar.getInstance();
	     calendar.setTimeInMillis(milliSeconds);
	     return formatter.format(calendar.getTime());
	}
	public String getDate(long milliSeconds, String dateFormat) {

	    SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

	     Calendar calendar = Calendar.getInstance();
	     calendar.setTimeInMillis(milliSeconds);
	     return formatter.format(calendar.getTime());
	}

	public Bitmap decodeImageFile(String path) {
		try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            final int REQUIRED_SIZE = 60;

            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;


            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
	}
	public String getDuration(int duration) {
		String time = "";
		duration /= 1000;
		int minutes = duration / 60;
		duration %= 60;
		if (minutes > 0) {
			time += minutes + " mins ";
		}
		time += duration + " secs";
		return time;
	}
	public Bitmap getAlbumart(int albumId, Context context) {
	    Bitmap bm = null;
	    try {
	        final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
	        Uri uri = ContentUris.withAppendedId(sArtworkUri, albumId);
	        ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(uri, "r");
            if (pfd != null) {
	            FileDescriptor fd = pfd.getFileDescriptor();
	            bm = BitmapFactory.decodeFileDescriptor(fd);
	        }
	    } catch (Exception e) {
	    }
	    return bm;
	}
	public String getSize(int size) {
		size /= 1024; 
		return size + "KB";
	}
	public String getSize(long size) {
		size /= 1024; 
		return size + "KB";
	}
}
