package main.emptycore.pcclicker;

public interface CallbackReceiver {
     public void receiveData (Object result);
}
